import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   Tnode (String name){
      this.name = name;
   }


   @Override
   public String toString() {
      return firstChild == null ? String.valueOf(name) :
              ( name + "(" + firstChild + "," + nextSibling + ")");
   }

   public static Tnode buildFromRPN (String pol) {
      Stack<Tnode> stack = new Stack<>();

      String[] listExp = pol.split(" ");


      for (String s: listExp) {
         switch (s){
            case "DUP":
               if (stack.isEmpty()) throw new IllegalArgumentException("Not enough items in tree for expression " + pol);
               Tnode peek = deepCopy(stack.peek());
               stack.push(peek);
//               try {
//                  Tnode an = stack.elementAt(stack.size() - 1);
//                  Tnode aa = an.clone();
//                  stack.push(aa);
//               } catch (CloneNotSupportedException e) {
//                  e.printStackTrace();
//               }
               continue;
            case "SWAP":
               if (stack.size() < 2) throw new IllegalArgumentException("Not enough items in tree for expression " + pol);
               Tnode first = stack.pop();
               Tnode second = stack.pop();
               stack.push(first);
               stack.push(second);
               continue;
            case "ROT":
               if (stack.size() < 3) throw new IllegalArgumentException("Not enough items in tree for expression " + pol);
               Tnode elem = stack.elementAt(stack.size() - 3);
               stack.removeElementAt(stack.size() - 3);
               stack.push(elem);
               continue;
         }
         if (s.matches("[a-zA-Z]+")) throw new IllegalArgumentException("Illegal argument in expression " + pol);
         Tnode tnode = new Tnode(s);
         if ("+-*/".contains(s)){
            if (stack.size() < 2) throw new IndexOutOfBoundsException("Not enough items in expression " + pol);
            tnode.nextSibling = stack.pop();
            tnode.firstChild = stack.pop();
         }
         stack.push(tnode);
      }

      if (stack.size() != 1) throw new IllegalArgumentException("Not enough arithmetic operations in expression " + pol);

      return stack.pop();

   }

   public static Tnode deepCopy(Tnode obj) {

      Tnode tnode = new Tnode(obj.name);

      if (obj.firstChild != null){
         tnode.firstChild = deepCopy(obj.firstChild);
      }
      if (obj.nextSibling != null){
         tnode.nextSibling = deepCopy(obj.nextSibling);
      }
      return tnode;
   }


   public static void main (String[] param) {
      String rpn = "2 5 SWAP +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      rpn = "3 DUP *";
      System.out.println ("RPN: " + rpn);
      res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      rpn = "2 5 9 ROT - +";
      System.out.println ("RPN: " + rpn);
      res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      rpn = "2 5 9 ROT + SWAP -";
      System.out.println ("RPN: " + rpn);
      res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      rpn = "2 5 DUP ROT - + DUP *";
      System.out.println ("RPN: " + rpn);
      res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      rpn = "-3 -5 -7 ROT - SWAP DUP * +";
      System.out.println ("RPN: " + rpn);
      res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      rpn = "3 5 + 8 - 9 / 22 3 + /";
      System.out.println("RPN: " + rpn);
      res = buildFromRPN(rpn);
      System.out.println("Tree: " + res);
   }
}

